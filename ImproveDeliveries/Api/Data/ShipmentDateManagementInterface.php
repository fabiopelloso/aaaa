<?php
declare(strict_types=1);

namespace VanMoof\ImproveDeliveries\Api\Data;


interface ShipmentDateManagementInterface
{
    public const VALID_SHIPMENT_DAYS = [7, 14, 30];
    public const DATE_FORMAT = 'd-m-Y';

    public function getDateByDay(int $days): string;
}
