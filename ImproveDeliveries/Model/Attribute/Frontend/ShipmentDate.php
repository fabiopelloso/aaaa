<?php
declare(strict_types=1);

namespace VanMoof\ImproveDeliveries\Model\Attribute\Frontend;

use Magento\Framework\DataObject;

use Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend;

use VanMoof\ImproveDeliveries\Api\Data\ShipmentDateManagementInterface;


class ShipmentDate extends AbstractFrontend
{
    private ShipmentDateManagementInterface $dateManager;

    /**
     * ShipmentDate constructor.
     *
     * @param  ShipmentDateManagementInterface  $dateManager
     */
    public function __construct(
        ShipmentDateManagementInterface $dateManager
    ) {
        $this->dateManager = $dateManager;
    }

    /**
     * @param  \Magento\Framework\DataObject  $object
     *
     * @return string
     */
    public function getValue(DataObject $object): string
    {
        $days = (int) $object->getData($this->getAttribute()->getAttributeCode());
        return $this->dateManager->getDateByDay($days);
    }
}
