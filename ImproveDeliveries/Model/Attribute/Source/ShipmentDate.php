<?php
declare(strict_types=1);

namespace VanMoof\ImproveDeliveries\Model\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

use VanMoof\ImproveDeliveries\Api\Data\ShipmentDateManagementInterface;


class ShipmentDate extends AbstractSource
{
    /**
     * @return array|null
     */
    public function getAllOptions(): ?array
    {
        if (!$this->_options) {

            $this->_options[] = ['label' => __('None'), 'value' => 0];

            foreach (ShipmentDateManagementInterface::VALID_SHIPMENT_DAYS as $day) {
                $this->_options[] = [
                    'label' => __(sprintf('%u days', $day)),
                    'value' => $day,
                ];
            }
        }

        return $this->_options;
    }
}
