<?php
declare(strict_types=1);

namespace VanMoof\ImproveDeliveries\Model\Attribute\Backend\Webapi;

use VanMoof\ImproveDeliveries\Api\Data\ShipmentDateManagementInterface;


class ShipmentDate extends \VanMoof\ImproveDeliveries\Model\Attribute\Backend\ShipmentDate
{
    private ShipmentDateManagementInterface $dateManager;

    public function __construct(
        ShipmentDateManagementInterface $dateManager
    ) {
        $this->dateManager = $dateManager;
    }

    public function afterLoad($object)
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $days = (int) $object->getData($attributeCode);
        $deliveryDate = $this->dateManager->getDateByDay($days);
        $object->setData($attributeCode, $deliveryDate);
        return parent::afterLoad($object);
    }
}