<?php
declare(strict_types=1);

namespace VanMoof\ImproveDeliveries\Model\Attribute\Backend;

use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;

use Magento\Framework\DataObject;

use Magento\Framework\Exception\LocalizedException;

use VanMoof\ImproveDeliveries\Api\Data\ShipmentDateManagementInterface;


class ShipmentDate extends AbstractBackend
{
    /**
     * @param  DataObject  $object
     *
     * @return bool
     * @throws LocalizedException
     */
    public function validate($object): bool
    {
        $value = (int) $object->getData($this->getAttribute()->getAttributeCode());

        if (!in_array($value, ShipmentDateManagementInterface::VALID_SHIPMENT_DAYS, true)) {
            throw new LocalizedException(__('Invalid value for shipment date. Valid values: ' . implode(',', ShipmentDateManagementInterface::VALID_SHIPMENT_DAYS)));
        }

        return true;
    }
}
