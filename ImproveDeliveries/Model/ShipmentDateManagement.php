<?php
declare(strict_types=1);

namespace VanMoof\ImproveDeliveries\Model;
use VanMoof\ImproveDeliveries\Api\Data\ShipmentDateManagementInterface;

class ShipmentDateManagement implements ShipmentDateManagementInterface
{
    /**
     * @param  int  $days
     *
     * @return string
     * @throws \Exception
     */
    public function getDateByDay(int $days): string
    {
        if (empty($days)) {
            return '';
        }

        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $date->modify(sprintf('+%s days', $days));
        return $date->format(ShipmentDateManagementInterface::DATE_FORMAT);
    }
}