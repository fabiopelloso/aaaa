<?php
declare(strict_types=1);

namespace VanMoof\ImproveDeliveries\Setup\Patch\Data;

use Magento\Catalog\Model\Product;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetupFactory;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;

use VanMoof\ImproveDeliveries\Model\Attribute\Backend\ShipmentDate as BackendShipmentDate;
use VanMoof\ImproveDeliveries\Model\Attribute\Frontend\ShipmentDate as FrontendShipmentDate;
use VanMoof\ImproveDeliveries\Model\Attribute\Source\ShipmentDate;


class CreateProductShipmentAttribute implements
    DataPatchInterface,
    PatchRevertableInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * CreateProductShipmentAttribute constructor.
     *
     * @param  ModuleDataSetupInterface  $moduleDataSetup
     * @param  EavSetupFactory  $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @return CreateProductShipmentAttribute|void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $eavSetup = $this->eavSetupFactory->create();
        $eavSetup->addAttribute(
            Product::ENTITY,
            'shipment_date',
            [
                'group' => 'General',
                'type' => 'int',
                'label' => 'Shipment Date',
                'input' => 'select',
                'source' => ShipmentDate::class,
                'frontend' => FrontendShipmentDate::class,
                'backend' => BackendShipmentDate::class,
                'required' => false,
                'sort_order' => 50,
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'visible' => true,
                'is_html_allowed_on_front' => true,
                'visible_on_front' => true,
            ]
        );

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    public function revert(): void
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $eavSetup = $this->eavSetupFactory->create();
        $eavSetup->removeAttribute(Product::ENTITY, 'shipment_date');

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }
}
